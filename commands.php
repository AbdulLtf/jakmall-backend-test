<?php

return [
    Commands\Add::class,
    Commands\Subtract::class,
    Commands\Multiply::class,
    Commands\Divide::class,
];
