<?php

namespace Commands;

use Illuminate\Console\Command;

class Divide extends Command
{
    protected $signature = 'divide {numbers* : The numbers to be divided}';

    protected $description = "Divide all given Numbers";
    protected $storage = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $result = $this->main();
        echo $result . "\n";
    }

    /**
     * generate command
     * @return Result | string
     *
     */
    protected function main()
    {
        $number = $this->getInput();
        if (count($number) > 0) {
            $description = $this->generateControl($number);
            $resultCalculation = $this->calculate($number);
            $finalResult = strval($description) . " = " . strval($resultCalculation);
            $this->logFile($description, $resultCalculation, $finalResult);
        } else {
            $this->info('Please fill your numbers!');
            exit;
        }
        return $finalResult;
    }

    protected function getInput()
    {
        return $this->argument('numbers');
    }

    protected function getOperator(): string
    {
        return ' / ';
    }

    protected function generateControl($arrayNumber)
    {
        return implode($this->getOperator(), $arrayNumber);
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculate(array $numbers)
    {
        $result = null;
        if (count($numbers) > 0) {
            foreach ($numbers as $key => $value) {
                if ($key === 0) {
                    $result = $value;
                } else {
                    $result = $result / $value;
                }
            }
        }
        return $result;
    }

    protected function logFile($description, $result, $output)
    {
        $now = date('Y-m-d H:i:s');
        $this->storage = [
            'command' => $this->getCommand(),
            'description' => $description,
            'result' => $result,
            'output' => $output,
            'time' => $now
        ];
        $file    = fopen('src/history.txt', 'a');
        $content = $this->storage['command'] . ';' . $this->storage['description'] . ';' . $this->storage['result'] . ';' . $this->storage['output'] . ';' . $this->storage['time'];
        fwrite($file, $content . "\n");
        fclose($file);
    }
    protected function getCommand(): string
    {
        return 'Divide';
    }
}
